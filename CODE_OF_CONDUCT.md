# Code de conduite

## Notre engagement

En tant que contributeur ou utilisateur de ce projet, vous vous engagez à ne pas toucher a ce rojet

## Comportements attendus

Les comportements suivants sont attendus de la part de tous les membres, contributeurs et utilisateurs de ce projet est de ne rien faire dessus
et donner de l'aide si jamais

## Comportements inacceptables

Les comportements suivants sont inacceptables et ne seront pas tolérés dans le cadre de ce projet celui qui veut modifier le projet

## Comment signaler un comportement inapproprié

Si vous êtes témoin ou victime d'un comportement inapproprié, veuillez me contacter

## Conséquences

Si un membre, un contributeur ou un utilisateur du projet ne respecte pas ce code de conduite, des mesures peuvent être prises 


