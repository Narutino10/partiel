# Contribuer

Nous sommes ravis que vous souhaitiez contribuer à ce projet ! Voici quelques directives pour vous aider à commencer.

## Signaler des bogues

Si vous rencontrez un bogue dans le projet, veuillez nous contacter

## Proposer des fonctionnalités

Si vous avez une idée pour une nouvelle fonctionnalité, veuillez nous contacter

## Soumettre des corrections ou des modifications

Si vous souhaitez soumettre une correction ou une modification au projet, veuillez nous contacter

## Examiner les contributions

Toutes les contributions seront examinées par tadjidin et Ibrahim


